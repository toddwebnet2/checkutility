function engageDataTable(idLocator, options) {
    $(idLocator).DataTable(options);
    $('.dataTables_length').addClass('bs-select');
}
