<?php
return [
    'loginHeader' => 'Iniciar Sesión',
    'login' => 'Iniciar',
    'appName' => 'FEI Enterprises',
    'username' => 'Usuario',
    'password' => 'Contraseña',

    'user_administration' => 'Administrador de Usuario',
    'positive_pay' => 'Positive Pay',
    'commissions' => 'Comisiones',
    'commissions_upload' => 'Enviar comisiones',
    'proveedores' => 'Proveedores',
    'cobranza' => 'Cobranza',
];
