<?php

return [
    'invalidCredentials' => 'Credenciales no válidas.',
    'success' => 'Éxito',
    'error' => 'Error',
    'info' => 'Nota',
    'warning' => 'Advertencia',
    'loggedOut' => 'Has sido desconectado.',
];
