<?php

return [
    'invalidCredentials' => 'Invalid Credentials.',
    'success' => 'Success',
    'error' => 'Error',
    'info' => 'Note',
    'warning' => 'Warning',
    'loggedOut' => 'You have been logged out.',
];
