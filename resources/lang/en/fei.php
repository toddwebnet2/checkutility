<?php

return [
    'loginHeader' => 'Login',
    'login' => 'Login',
    'appName' => 'FEI Enterprises',
    'username' => 'User Name',
    'password' => 'Password',

    'user_administration' => 'User Administration',
    'positive_pay' => 'Positive Pay',
    'commissions' => 'Commissions',
    'commissions_upload' => 'Upload Commissions',
    'proveedores' => 'Accounts Payable',
    'cobranza' => 'Collections'
];
