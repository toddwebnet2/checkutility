@extends('layouts.template')

@section('title',  "This Page" )

@section('body')
    <h1>Dashboard</h1>
    @foreach($links as $link)
        <div class="dashboard_link">
            <a href="{{ $link['link'] }}">
                {{ $link['label'] }}
            </a>
        </div>
    @endforeach
@endsection
