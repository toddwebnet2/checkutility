<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FEI - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=app_url();?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=app_url();?>/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=app_url();?>/css/app.css" rel="stylesheet">
    <script src="<?=app_url();?>/js/jquery.min.js"></script>
    <script src="<?=app_url();?>/js/jquery.numeric.min.js"></script>

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script type="text/javascript">
        // var curLocale = "?=\App\Services\LanguageService::getLanguage()?";
    </script>

</head>

<body style="margin: 0px">
<nav class="navbar navbar-toggleable  fixed-top
navbar fixed-top navbar-toggleable-xl
">
    <a class="navbar-brand" href="/">
        <img src="<?=app_url()?>/images/fei-logo-small.png" style="height:30px"/>
    </a>
</nav>
@if (Auth::isLoggedIn())
    <div class="userbar" style="text-align:right">
        {!! \App\Services\UserBarService::getUserBarInfo() !!}
    </div>
@endif
<div class="container" id="mainBody">
    {!! \App\Services\FlashService::handleFlashMessage() !!}
    <script>
        setTimeout(function () {
            var flashMessage = document.getElementById('flashMessage');
            if (flashMessage) {
                flashMessage.style.display = 'none';
            }
        }, 5000);
    </script>
    <div class="starter-template" style="margin-top: 40px; min-height: 600px">
        @section('body')
        @show()
    </div>
</div>

</div><!-- /.container -->

<div class="container" id="footer">
    <div class="col-md-12" id="footer-links">

        <a href="https://www.feifinancial.com">Home</a>
        &nbsp;&nbsp; |&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/fulfillment/default.asp">Fulfillment</a>
        &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/login-fei.asp">Comisiones</a>
        &nbsp;&nbsp; |&nbsp;&nbsp;&nbsp;
        <a href="https://app.feifinancial.com/login-fei.asp">Cobranza</a>

    </div>
    <div style="text-align:center"><img src="<?=app_url()?>/images/logo.png"/></div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="<?=app_url()?>/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug
<script src="/js/ie10-viewport-bug-workaround.js"></script>
-->

<link href="<?=app_url()?>/css/select2.min.css" rel="stylesheet"/>
<script src="<?=app_url()?>/js/select2.min.js"></script>
<script src="<?=app_url()?>/js/jquery.dataTables.min.js"></script>

<script src="<?=app_url()?>/js/fei.js"></script>
</body>
</html>
