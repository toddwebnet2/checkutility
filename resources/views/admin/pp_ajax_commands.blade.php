<h1 style="color: red">THIS IS NOT READY YET</h1>
<h3>Upload Data To Bank</h3>
<p align="center">
    Use this tool to upload the TEXT file named
    @if($bank == 'prosperity')
        &quot;<b>importFV.csv</b>&quot;
    @endif
    @if($bank == 'amegy')
        &quot;<b>import.csv</b>&quot;
    @endif
    file from your computer to the server.
<ol>
    <li>Simply click &quot;Browse&quot;</li>
    <li>Find the file on your computer.</li>
    <li>Select the file.</li>
    <li>Click &quot;Open&quot; or Double-Click.</li>
    <li>Click &quot;Upload&quot;<br>
        NOTE:&nbsp; The file must be named:
        @if($bank == 'prosperity')
            &quot;<b>importFV.csv</b>&quot;
        @endif
        @if($bank == 'amegy')
            &quot;<b>import.csv</b>&quot;
        @endif
    </li>
</ol>
<?php
if ($bank == 'prosperity') {
    $uploadPath = '/admin/pp/upload/prosperity';
    // $uploadPath = env('ROOT_PATH') . 'bank/prosperity/upload';
} else {
    $uploadPath = '/admin/pp/upload/amegy';
    // $uploadPath = '/pp/default_upload.asp';
}
?>
<form method="POST"
      ENCTYPE="multipart/form-data"
      action="{{ $uploadPath }}"
>
    {{ csrf_field() }}
    <p><input type="file"
              name="F1"
              size="38"
        ></p>
    <p><input type="submit"
              value="Upload"
              name="B1"
        ><input type="reset"
                value="Reset"
                name="B2"
        ></p>
</form>
</p>


Process File:&nbsp
@if($bank == 'prosperity')
    <a class="btn-primary btn"
       href="https://app.feifinancial.com/pp/ppSQLCobrado.asp?bankid=FV"
    >Prosperity</a>
    (importFV.csv)
@endif
@if($bank == 'amegy')
    <a class="btn-primary btn"
       href="https://app.feifinancial.com/pp/ppSQLCobrado.asp?bankid="
    >Amegy</a>
    (import.csv)
@endif

<hr>
<h1 style="color: red">THIS IS NOT READY YET</h1>
<h3>Retrieve Data For Bank</h3><BR>
Retrieve Data File:&nbsp
@if($bank == 'prosperity')
    <a class="btn-primary btn"
       href="/admin/pp/get/prosperity"
    >Prosperity</a>
    (importFV.csv)
@endif
@if($bank == 'amegy')
    <a class="btn-primary btn"
       href=/admin/pp/get/amegy"
    >Amegy</a>
    (import.csv)
@endif


<BR><BR>then
Save the file to your computer

@if($bank == 'prosperity')
    <form method="POST"
          name="frmMain2"
          target="_blank"
          action="http://www.prosperitybanktx.com"
    >
        <img border="0"
             src="/images/prosperity.gif"
             width="257"
             height="59"
        ><input
            type="submit"
            value="Sign On"
            name="subSignOn"
        >
        <input type="hidden"
               name="txtEntity"
               value="FEIEnter"
        >

    </form>
@endif
@if($bank == 'amegy')
    <form method="POST"
          name="frmMain3"
          target="_blank"
          action="https://edelivery.amegybank.com/NetStar/signon.aspx"
    >
        &nbsp;<input type="hidden"
                     name="txtEntity"
                     value="FEIEnter"
        ><img border="0"
              src="/images/Amegy.jpeg"
              width="257"
              height="59"
        ><input type="submit"
                value="Sign On"
                name="subSignOn2"
        >
    </form>
@endif


<BR>
<a href="/admin/pp/update-sql"
   class="btn btn-primary"
>Update SQL</a>
