@extends('layouts.template')

@section('title',  "FEI Positive Pay" )

@section('body')
    <h1>Positive Pay Banking</h1>
    <hr>
    <?=\App\Services\FlashService::handleFlashMessage()?>
    <div class="row">
        <div class="col-md-6" style="border: 2px solid #999; border-right: 0px">
            <h2 style="text-align:center">Prosperity Bank</h2>
            <div id="ProsperityContainer"></div>
        </div>
        <div class="col-md-6" style="border: 2px solid #999; ">
            <h2 style="text-align:center">Amegy Bank</h2>
            <div id="AmegyContainer"></div>
        </div>

    </div>


    <script type="text/javascript">
        $(document).ready(function (){
            $('#ProsperityContainer').load('/admin/ajax/commands/prosperity');
            $('#AmegyContainer').load('/admin/ajax/commands/amegy');

        });
    </script>
@endsection
