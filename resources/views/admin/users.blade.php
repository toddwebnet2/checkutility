@extends('layouts.template')

@section('title',  __('fei.user_administration') )

@section('body')
    <a href="/admin/user/company/0" class="btn btn-primary float-right" style="margin-top: 10px">Add User</a>
    <h2 style="padding-bottom: 1px;">{{ __('fei.user_administration') }}</h2>


    <table class="sortTable table-bordered table table-striped">
        <thead>
        <tr>
            <th>Company ID</th>
            <th>Company</th>
            <th>User Name</th>
            <th>Permissions</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)
            <?php
            $users = $company->users;
            $flag = false;
            ?>
            @foreach($users as $user)
                <?php $flag = true;
                ?>
                <tr>
                    <td><a href="/admin/company/{{ $company->id }}">{{ $company->company_code }}</a>
                    </td>
                    <td>{{ $company->company_name }}
                        @if($company->image)
                            <img src="/admin/image/{{ $company->image->id }}"
                                 style="float:right; height: 40px"/>
                        @endif
                    </td>
                    <td>{{ $user->username }}</td>
                    <td>
                        {{ $user->permissionShortList() }}
                    </td>
                    <td style="text-align:center">
                        <a href="/admin/user/{{ $user->id }}" class="btn btn-primary">Edit</a></td>
                </tr>
            @endforeach
            @if(!$flag)
                <tr>
                    <td><a href="/admin/user/company/{{ $company->id }}">{{ $company->company_id }}</a>
                    <td>{{ $company->company_name }}</td>
                    <td> - none -</td>
                    <td>&nbsp;</td>
                    <td style="text-align:center"><a href="/admin/user/company/{{ $company->id }}"
                                                     class="btn btn-primary">Add</a></td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            engageDataTable('.sortTable', {paging: false})

        });
    </script>
@endsection
