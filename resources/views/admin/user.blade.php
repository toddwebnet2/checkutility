@extends('layouts.template')

@section('title',  __('fei.user_administration') )

@section('body')

    <h2>{{ __('fei.user_administration') }}</h2>
    <a href="/admin/users">User List</a>
    <form action="/admin/user/" method="post" id="companyForm" onsubmit="return companyFormValidate()"
          enctype="multipart/form-data" style="margin-left:150px">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id" value="{{ $user->id }}"/>
        <input type="hidden" name="image_id" id="image_id" value="<?php
        if ($user->company->image === null) {
            print 0;
        } else {
            print $user->company->image->id;
        }
        ?>"/>
        <table>

            <tr>
                <td><b> <label for="company_id">Company: </label></b></td>
                <td><select class="select2" id="company_id" name="company_id" onchange="companyIdChange()"
                            style="width: 450px">
                        @foreach($companies as $company)
                            <option
                                value="{{ $company->id }}" <?=$user->company_id == $company->id ? "selected" : ""?>>{{ $company->company_id  }}
                                ({{ $company->company_code }}) {{$company->company_name}}
                            </option>
                        @endforeach
                        <option value="-1" <?=$user->company_id == 0 ? "selected" : ""?>>New Company</option>
                    </select></td>
            </tr>
            <tr>
                <td><b>
                        <label for="company_name">Company Name: </label>

                    </b></td>
                <td><input type="text" value="" id="company_name" name="company_name" style="width: 450px"/></td>
            </tr>
            <tr>
                <td><b><label for="company_code">Company ID: </label></b></td>
                <td><input type="text" value="" id="company_code" name="company_code"/></td>
            </tr>
            <tr>
                <td><b><label for="image_description">Image Description: </label></b></td>
                <td><input type="text" value="" id="image_description" name="image_description" style="width:450px"/></td>
            </tr>
            <tr>
                <td><b><label for="image">Image: </label></b></td>
                <td><input type="file" value="" id="image" name="Image"/>
                    <BR>Current Image:<BR>
                    <img id="CurrentImage" style="height:80px"/></td>
            </tr>
            <tr>
                <td><b> <label for="username">User ID: </label></b></td>
                <td><input type="text" value="{{ $user->username }}" id="username" name="username"/></td>
            </tr>
            <tr>
                <td><b><label for="password">Password: </label></b></td>
                <td><input type="text" value="{{ $user->password }}" id="password" name="password"/></td>
            </tr>
            <tr>
                <td><b>&nbsp;</b></td>
                <td><b>&nbsp;</b></td>
            </tr>
            <tr>
                <td><b>&nbsp;</b></td>
                <td>
                    <input type="button" class="btn btn-warning" value="Cancel"
                           onclick="document.location='/admin/users';"/>
                    <input type="submit" class="btn btn-primary" value="Save" style="margin-left:100px"/>


                </td>
            </tr>

        </table>


    </form>
    <script>
        var customers = JSON.parse('<?=$companies->load('image')->toJSON()?>');

        $(document).ready(function () {
            companyIdChange();
            $('#companyId').select2();
        });

        function companyIdChange() {
            customerID = $('#company_id').val();
            if (customerID == -1) {
                $('#company_name').val('');
                $('#company_code').val('');
                $('#image_id').val(0);
                $('#image_description').val('');
                $('#CurrentImage').attr('src', '/images/no-image-available.jpg');
            } else {
                customer = customers[customerID];
                $('#company_name').val(customer.company_name);
                $('#company_code').val(customer.company_code);
                loadImageDescription(customerID);
                loadImageId(customerID);
                if (customer.image === null) {
                    $('#image_description').val('');
                    $('#CurrentImage').attr('src', '/images/no-image-available.jpg');
                } else {
                    $('#image_description').val(customer.image.label);
                    $('#CurrentImage').attr('src', '/admin/image/' + customer.image.id);
                }
            }
        }

        function loadImageId(customerId) {
            jQuery.ajax({
                url: '/image/id/' + customerId,
                type: "GET",
                cache: false,
            }).done(function (data) {
                $('#ImageID').val(data);
            });
        }

        function loadImageDescription(customerId) {
            jQuery.ajax({
                url: '/image/description/' + customerId,
                type: "GET",
                cache: false,
            }).done(function (data) {
                $('#ImageDescription').val(data);
            });
        }

        function companyFormValidate() {
            errs = [];
            if ($('#CompanyName').val() == '') {
                errs.push("Company Name cannot be empty");
            }
            if ($('#CompanyCode').val() == '') {
                errs.push("Company ID cannot be empty");
            }
            if ($('#ImageDescription').val() == '') {
                errs.push("Image Description cannot be empty");
            }
            if ($('#UserID').val() == '') {
                errs.push("User ID cannot be empty");
            }
            if ($('#Password').val() == '') {
                errs.push("Password cannot be empty");
            }
            // errs.push("Testing");
            if (errs.length > 0) {
                alert(errs.join("\n\n"));
                return false;
            }
            return true;
        }
    </script>
@endsection
