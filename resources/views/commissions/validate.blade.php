@extends('layouts.template')

@section('title',  __('fei.commissions_upload') )

@section('body')
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 40px">
        <tr>
            <td style="background-image: url('https://app.feifinancial.com/images/main-menu-bg.png');

background-repeat:no-repeat; background-position: top; height:99px;">
                <table width="90%" border="0" align="center" cellpadding="5" cellspacing="0" style="margin-top:-30px;">
                    <tr>
                        <td width="28%" height="50" align="center"
                            style="font-family:Arial, Helvetica, sans-serif; color: #003466; text-transform: uppercase; font-size: 18px; font-weight: 600;">
                            {{ $companyName }} </b>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    @if(count($errors) +  count($dataErrors) > 0)
        <div class="alert alert-danger">Cannot Proceed due to errors in the data<BR>
            <?php

            foreach ($errors as $error) {
                print "{$error}<bR>";
            }
            ?></div>
    @endif


    @if(count($data)>0)

        @if(is_array($dataErrors) && count($dataErrors))
            <div class="alert alert-danger">Cannot Proceed due to errors in the data<BR>
                <?php foreach ($dataErrors as $line => $errors) {
                    print "<b>Line: {$line}<BR>";
                    foreach ($errors as $error) {
                        print "{$error}<br>";
                    }
                }?></div>
            <br/><a style="float:right" href="/commissions/upload">Back</a>
        @else
                <div class="alert alert-info">
                     ${{ formatNumber($total) }} Total
                    <span style="float:right">{{ $numRows }} Entries</span>
                </div>
            <form action="/commissions/validate" method="post">
                {{ csrf_field() }}

                <input type="hidden" name="company_id" value="{{ $companyId }}"/>
                <input type="hidden" name="total_records" value="{{ $numRows }}"/>
                <input type="hidden" name="total_amount" value="{{ $total }}"/>
                <input type="hidden" name="upload_file_id" value="{{ $uploadedFileId }}"/>

                <br/>
                <a style="float:right" href="/commissions/upload">Back</a>
                <input type="submit" class="btn btn-primary" value="Finalize"/>
                <br/><br/>
            </form>
        @endif

        <table style="margin: auto; font-size: 11px;" border="1">
            <tr>
                <th>&nbsp;</th>
                @foreach($data[0] as $key=>$value)
                    <th>{{ $key }}</th>
                @endforeach

            </tr>

            <?php $c = 0;?>
            @foreach($data as $row)
                <?php $c++?>
                <tr <?=(array_key_exists($c, $dataErrors)) ? 'class="alert alert-danger"' : ''?>>
                    <td>{{ $c }}
                    </td>
                    @foreach($row as $key=>$value)
                        <td>{{ $value }}</td>
                    @endforeach
                </tr>
                @if(array_key_exists($c, $dataErrors))
                    <tr class="alert alert-danger">
                        <td>&nbsp;</td>
                        <td colspan="<?=count($row)?>"><?=implode($dataErrors[$c])?></td>
                    </tr>
                @endif

            @endforeach
        </table>
        <BR><BR><a class="float-right" href="/commissions/upload">Back</a><BR><BR>

    @endif
    @if (count($data)==0 && count($errors)==0)
        <div class="alert alert-danger">Cannot Proceed no data received</div>
    @endif
@endsection
