@extends('layouts.template')

@section('title',  __('fei.commissions_upload') )

@section('body')

    <?php
        $keys= [
            "REF",
            "AGENCY",
            "AMOUNT",
            "ADDRESS1",
            "ADDRESS2",
            "ADDRESS3",
            "CITY",
            "STATE",
            "ZIP",
            "COUNTRY",
            "HOTEL",
            "FIRST",
            "LAST",
            "IN",
            "OUT",
            "IATA"
        ];
    ?>

        <table style="margin: auto; font-size: 11px;" border="1">
            <tr>
                <th>&nbsp;</th>
                @foreach($keys as $key)
                    <th>{{ strtoupper($key) }}</th>
                @endforeach

            </tr>

            <?php $c = 0;?>
            @foreach($batch->details as $row)
                <?php $c++?>
                <tr>
                    <td>{{ $c }}
                    </td>
                    @foreach($keys as $key)
                        <td>{{ $row->{strtolower($key)} }}</td>
                    @endforeach
                </tr>

            @endforeach
        </table>
        <BR><BR><a class="float-right" href="/commissions/upload">Back</a><BR><BR>


@endsection
