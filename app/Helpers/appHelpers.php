<?php

function appPath($path)
{
    return app_path(str_replace('/', DIRECTORY_SEPARATOR, ltrim($path, '/')));
}

if (!function_exists('mime_content_type')) {
    function mime_content_type($filename)
    {
        $result = new finfo();

        if (is_resource($result) === true) {
            return $result->file($filename, FILEINFO_MIME_TYPE);
        }

        return false;
    }
}

function formatNumber($num, $decimals = 0)
{
    return number_format($num, $decimals, '.', ",");
}

function formatDate($date)
{
    return date('m/d/Y', strtotime($date));
}

function dbDate($date)
{
    return date('Y-m-d H:s:i', strtotime($date));
}


function app_url()
{
    return config('app.url');
}
