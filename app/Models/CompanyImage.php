<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyImage extends Model
{
    protected $fillable = [
        'company_id',
        'label',
        'path'
    ];

    public static function copyImage($tmpPath)
    {
        $fileName = self::randomChars(32);

        $fileType = self::getFileType($tmpPath);

        $targetPath = storage_path();
        $targetPath .= '/app';
        if (!file_exists($targetPath)) {
            mkdir($targetPath);
        }
        $targetPath .= '/companyImages';
        if (!file_exists($targetPath)) {
            mkdir($targetPath);
        }
        $targetPath .= '/' . $fileName . '.' . $fileType;
        rename($tmpPath, $targetPath);
        return $targetPath;
    }



    private static function getFileType($filePath)
    {
        $handle = @fopen($filePath, 'r');
        if (!$handle) {
            throw new \Exception('File Open Error');
        }

        $types = array(
            'jpeg' => "\xFF\xD8\xFF",
            'gif' => 'GIF',
            'png' => "\x89\x50\x4e\x47\x0d\x0a",
            'bmp' => 'BM',
            'psd' => '8BPS',
            'swf' => 'FWS'
        );
        $bytes = fgets($handle, 8);
        $found = 'other';
        foreach ($types as $type => $header) {
            if (strpos($bytes, $header) === 0) {
                $found = $type;
                break;
            }
        }
        fclose($handle);
        return $found;
    }

    private static function randomChars($num)
    {
        $chars = '';
        $availableChars = str_split(
            '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        );
        for ($x = 0; $x < $num; $x++) {
            $chars .= $availableChars[rand(0, count($availableChars) - 1)];
        }
        return $chars;
    }
}
