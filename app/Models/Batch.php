<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = [
        'company_id',
        'total_records',
        'uploaded_file_id',
        'total_amount',
        'file_path'
    ];

    public function details()
    {
        return $this->hasMany(BatchDetail::class);
    }
}
