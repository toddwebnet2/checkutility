<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'company_id',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'address3',
        'city',
        'state',
        'zip',
        'country',
        'phone1',
        'phone2',
        'fax',
        'email',
        'url',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
