<?php

namespace App\Models;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'contact_id',
        'company_id',
        'username',
        'password',
        'note',
        'language',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function roles()
    {
        return Role::whereIn('id', UserRole::where('user_id', $this->id)->pluck('role_id'))->get();
        //        return $this->hasManyThrough(Role::class, UserRole::class, 'role_id', 'id', 'id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Crypt::encrypt($value);
    }

    public function getPasswordAttribute($value)
    {
        return Crypt::decrypt($value);
    }

    public static function isValidLogin($username, $password)
    {
        $user = User::where('username', $username)->first();
        if ($user !== null && $user->password == $password) {
            return true;
        }
        return false;
    }

    public function permissions()
    {
        static $permissions;
        if ($permissions === null) {
            $permissions = [];
            foreach ($this->roles() as $role) {
                foreach ($role->permissions as $permission) {
                    if (!in_array($permission, $permissions)) {
                        $permissions[] = $permission;
                    }
                }
            }
        }
        return $permissions;
    }

    public function permissionShortList(){
        $list = [];
        foreach($this->permissions() as $permission){
            $abbr = '';
            foreach(explode('-', $permission) as $name){
                $abbr.= substr(ucfirst($name), 0, 1);
            }
            $list[] = $abbr;

        }
        return implode(', ', $list);
    }


}
