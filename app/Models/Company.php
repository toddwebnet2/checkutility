<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = [
        'company_code',
        'company_name',
        'qb_list_id',
    ];

    public function quickBooks()
    {
        $this->hasOne(QbCustomer::class, 'qb_list_id', 'qb_ist_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function image()
    {
        return $this->hasOne(CompanyImage::class);
    }

    public static function getByCompanyCode($companyCode)
    {
        return self::where('company_code', $companyCode)->first();
    }
}
