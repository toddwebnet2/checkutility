<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QbCustomer extends Model
{
    protected $table = 'qb_customers';
    public $timestamps = false;
    protected $fillable = [
        'qb_list_id',
        'company_name'
    ];
}
