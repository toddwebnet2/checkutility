<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchDetail extends Model
{
    protected $fillable = [
        'batch_id',
        'ref',
        'agency',
        'amount',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'address3',
        'city',
        'state',
        'postal_code',
        'country',
        'iata',
        'in',
        'out',
        'created_at',
        'updated_at',
    ];
}
