<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportLookup extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'source',
        'source_id',
        'target',
        'target_id'
    ];
}
