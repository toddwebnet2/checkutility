<?php

namespace App\Services;

use App\Models\Batch;
use App\Models\BatchDetail;
use App\Models\Company;
use App\Traits\Singleton;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class UploadService
{
    use Singleton;

    public function validateFileType(UploadedFile $file)
    {
        $validTypes = [
            'csv' => ['text/csv'],
            'xls' => ['application/vnd.ms-excel'],
            'xlsx' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        ];
        $ext = $file->getClientOriginalExtension();
        $mimeType = $file->getClientMimeType();
        return in_array($ext, array_keys($validTypes)) && in_array($mimeType, $validTypes[$ext]);
    }

    public function transformUpload(UploadedFile $file)
    {
        $data['companyCode'] = AuthService::sessionVar('companyCode');
        $company = Company::where('company_code', $data['companyCode'])->first();
        $data['companyId'] = $company->id;
        $data['companyName'] = $company->company_name;
        $data['errors'] = [];
        $data['data'] = [];
        $ext = $file->getClientOriginalExtension();
        switch ($ext) {
            case 'csv':
                $data = $this->transformCsv($file, $data);
                break;
            case 'xls':
            case 'xlsx':
                $data = $this->transformXls($file, $data);
                break;
            default:
                dd('stop');
        }
        if (count($data['errors']) + count($data['dataErrors']) == 0) {
            $uploadedFile = \App\Models\UploadedFile::create(
                [
                    'company_id' => $company->id,
                    'file_path' => $data['savedPath'],
                    'data' => $data['data']
                ]
            );
            $data['uploadedFileId'] = $uploadedFile->id;
        }
        unset($data['savedPath']);
        return $data;
    }

    private function transformCsv(UploadedFile $file, $data)
    {
        $csvService = new CsvReaderService();
        $rootPath = $file->getRealPath();
        $csvService->loadFile($rootPath, ",");

        $csvService->validateHeaders($this->requiredHeaders(), $this->requiredHeaders());
        if (count($csvService->errors())) {
            $data['errors'] = $csvService->errors();
        } else {
            do {
                $row = $csvService->next();
                if ($this->isValidRow($csvService->getHeaders(), $row)) {
                    $data['data'][] = $row;
                }
            } while ($row !== false);
            $data['dataErrors'] = $this->processForErrors($data['data']);
        }
        $ts = date("Y-m-G-i-s");
        $passFail = (
            count($data['dataErrors']) + count($data['errors']) == 0
        ) ? "pass" : "fail";
        $fileName = "{$data['companyCode']}_{$ts}_{$passFail}.csv";
        $saveToPath = storage_path() . DIRECTORY_SEPARATOR . implode(
                DIRECTORY_SEPARATOR,
                explode('/', "app/uploads/single/{$fileName}")
            );
        File::copy($rootPath, $saveToPath);
        $data['savedPath'] = $saveToPath;
        return $data;
    }

    private function transformXls(UploadedFile $file, $data)
    {
        $xlsService = new ExcelReaderService();
        $rootPath = $file->getRealPath();
        $xlsService->loadFile($rootPath);

        $xlsService->validateHeaders($this->requiredHeaders(), $this->requiredHeaders());
        if (count($xlsService->errors())) {
            $data['errors'] = $xlsService->errors();
        } else {
            do {
                $row = $xlsService->next();
                if ($this->isValidRow($xlsService->getHeaders(), $row)) {
                    $data['data'][] = $row;
                }
            } while ($row !== false);
            $data['dataErrors'] = $this->processForErrors($data['data']);
        }
        $ts = date("Y-m-G-i-s");
        $passFail = (
            count($data['dataErrors']) + count($data['errors']) == 0
        ) ? "pass" : "fail";
        $fileName = "{$data['companyCode']}_{$ts}_{$passFail}.csv";
        $saveToPath = storage_path() . DIRECTORY_SEPARATOR . implode(
                DIRECTORY_SEPARATOR,
                explode('/', "app/uploads/single/{$fileName}")
            );
        File::copy($rootPath, $saveToPath);
        $data['savedPath'] = $saveToPath;
        return $data;
    }

    private function requiredHeaders()
    {
        return [
            "REF",
            "AGENCY",
            "AMOUNT",
            "ADDRESS1",
            "ADDRESS2",
            "ADDRESS3",
            "CITY",
            "STATE",
            "ZIP",
            "COUNTRY",
            "HOTEL",
            "FIRST",
            "LAST",
            "IN",
            "OUT",
            "IATA"
        ];
    }

    private function processForErrors($data)
    {
        $errors = [];
        foreach ($data as $index => $row) {
            $err = [];
            if (trim($row['REF']) == '') {
                $err[] = "REF is blank";
            }
            if (trim($row['AGENCY']) == '') {
                $err[] = "AGENCY is blank";
            }
            if (trim($row['AMOUNT']) == '' || !is_numeric($row['AMOUNT'])) {
                $err[] = "AMOUNT must be a number";
            }
            if (trim($row['CITY']) == '') {
                $err[] = "CITY is blank";
            }
            if (trim($row['STATE']) == '') {
                $err[] = "STATE is blank";
            }
            if (trim($row['ZIP']) == '') {
                $err[] = "ZIP is blank";
            }
            if (trim($row['COUNTRY']) == '') {
                $err[] = "COUNTRY is blank";
            }
            if (trim($row['HOTEL']) == '') {
                $err[] = "HOTEL is blank";
            }
            if (trim($row['FIRST']) == '') {
                $err[] = "FIRST is blank";
            }
            if (trim($row['LAST']) == '') {
                $err[] = "LAST is blank";
            }
            if (trim($row['IN']) == '' || !$this->isValidDate($row['IN'])) {
                $err[] = "IN must be a valid date";
            }
            if (trim($row['OUT']) == '' || !$this->isValidDate($row['IN'])) {
                $err[] = "OUT must be a valid date";
            }
            if (trim($row['IATA']) == '' || !is_numeric(trim($row['IATA']))) {
                $err[] = "IATA must be a valid number";
            } elseif (strlen(trim($row['IATA'])) > 8) {
                $err[] = "IATA is too long (max of 8 digits)";
            }

            if (count($err)) {
                $errors[$index + 1] = $err;
            }
        }
        return $errors;
    }

    private function isValidDate($date)
    {
        return strtotime($date) !== false;
    }

    private function isValidRow($headers, $row)
    {
        if ($row === false) {
            return false;
        }
        foreach ($headers as $key => $header) {
            if ($row[$header] !== false && trim($row[$header]) != '') {
                return true;
            }
        }
        return false;
    }

    public function postNewBatches($companyId, $totalRecords, $totalAmount, $uploadFileId)
    {
        $uploadedFile = \App\Models\UploadedFile::find($uploadFileId);
        $batch = Batch::create(
            [
                'upload_file_id' => $uploadFileId,
                'company_id' => $companyId,
                'total_records' => $totalRecords,
                'total_amount' => $totalAmount,
                'file_path' => $uploadedFile->file_path,
                'status_id' => 1,
            ]
        );
        foreach ($uploadedFile->data as $row) {
            $detail = ['batch_id' => $batch->id];
            foreach ($row as $key => $value) {
                $detail[strtolower($key)] = $value;
            }
            BatchDetail::create($detail);
        }
        return Batch::find($batch->id);
    }
}
