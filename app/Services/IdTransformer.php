<?php

namespace App\Services;


use Jenssegers\Optimus\Optimus;

// An attempt to make this a middleware (decode/encode IDs with it automatically) failed.
// Altering a request apparently goes against the design of Laravel anyway. See:
// https://github.com/laravel/framework/issues/10725#issuecomment-150930866
class IdTransformer
{
    public function encodeId($databaseId)
    {
        return $this->getTransformer()->encode($databaseId);
    }

    public function decodeId($encodedId)
    {
        return $this->getTransformer()->decode($encodedId);
    }

    private function getTransformer()
    {
        // For more info, see: https://github.com/jenssegers/optimus
        return new Optimus(26062297, 1892156009, 1995407126);
    }
}
