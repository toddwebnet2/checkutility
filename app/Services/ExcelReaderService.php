<?php

namespace App\Services;

use App\Helpers\ArrayHelpers;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelReaderService
{
    protected $headers;
    protected $curRow = 0;
    private $errors = [];
    private $fileLoaded;

    /** @var Worksheet */
    private $sheet = null;


    public function __construct()
    {
        // initialize the file has not been loaded
        $this->fileLoaded = false;
    }

    public function loadFile(string $xlsPath)
    {
        // load the file, get the headers and mark the starting row at 0

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($xlsPath);
        $this->sheet = $spreadsheet->getActiveSheet();
        $this->fileLoaded = true;
        $this->extractHeaders();
//        $this->reset();
    }

    private function extractHeaders()
    {
        // do nothing if not loaded
        if (!$this->fileLoaded) {
            return;
        }
        $col = 1;
        $blankCount = 0;
        $row = 1;
        $values = [];
        while ($blankCount < 3) {
            $colLetter = $this->columnLetter($col);
            $cell = "{$colLetter}{$row}";
            $value = $this->sheet->getCell($cell)->getValue();
            $values[] = $value;
            if (trim($value) == '') {
                $blankCount++;
            } else {
                $blankCount = 0;
            }
            $col++;
        }
        $headers = [];
        for ($x = 0; $x < count($values) - 3; $x++) {
            $headers[] = $values[$x];
        }
        $this->headers = $headers;
    }

    private function fetchRow($rowNum)
    {
        $col = 1;
        $row = $rowNum + 1;
        $values = [];
        $nullCount = 0;
        $rowCount = 0;
        for ($x = 0; $x < count($this->headers); $x++) {
            $colLetter = $this->columnLetter($col + $x);
            $cell = "{$colLetter}{$row}";
            $value = $this->sheet->getCell($cell)->getValue();
            $rowCount++;
            if ($value === null) {
                $nullCount++;
            }
            switch ($this->headers[$x]) {
                case "IN":
                case "OUT":
                    $value = Date('Y-m-d', Date::excelToTimestamp($value));
                    break;
                case "IATA":
                case "REF":
                    $value = (string)$value;
                    break;
            }
            $values[] = $value;
        }
        if($rowCount == $nullCount){
            return [];
        }
        return $values;
    }

    private function columnLetter($c)
    {
        $c = intval($c);
        if ($c <= 0) {
            return '';
        }

        $letter = '';

        while ($c != 0) {
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }

        return $letter;
    }

    public function reset()
    {
        $this->curRow = 0;
    }

    public function validateHeaders(array $allowedHeaders = [], array $requiredHeaders = [])
    {
        // validation
        $errs = [];

        if (!$this->fileLoaded) {
            $errs = ['File not loaded'];
        }
        // combine all error categories
        $this->errors = array_merge(
            $errs,
            $this->getHeaderDupeErrors(),
            $this->getHeaderRequiredErrors($requiredHeaders),
            $this->getHeaderOutlierErrors($allowedHeaders)
        );
        return (count($this->errors) == 0);
    }

    /**
     * determines if there are duplicate headers
     *
     * @return array
     */
    private function getHeaderDupeErrors()
    {
        if (!$this->fileLoaded) {
            return [];
        }
        $errs = [];
        $dupes = ArrayHelpers::getDupes($this->headers);

        if (count($dupes) > 0) {
            foreach ($dupes as $dupe) {
                $errs[] = 'Duplicate header: ' . $dupe;
            }
        }
        return $errs;
    }

    /**
     * returns errors if required headers are not present
     *
     * @param array $requiredHeaders
     * @return array
     */
    private function getHeaderRequiredErrors(array $requiredHeaders)
    {
        if (!$this->fileLoaded || $requiredHeaders == []) {
            return [];
        }
        $errs = [];
        foreach (array_diff($requiredHeaders, $this->headers) as $required) {
            $errs[] = 'Missing required header: ' . $required;
        }
        return $errs;
    }

    /**
     * returns errors for unexpected headers
     *
     * @param array $allowedHeaders
     * @return array
     */
    private function getHeaderOutlierErrors(array $allowedHeaders)
    {
        if (!$this->fileLoaded) {
            return [];
        }
        $errs = [];
        if (count($allowedHeaders) > 0) {
            foreach (array_diff($this->headers, $allowedHeaders) as $outlier) {
                $errs[] = 'Unexpected header: ' . $outlier;
            }
        }
        return $errs;
    }

    /**
     * returns errors property
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    public function next()
    {
        // no row if file not loaded
        if (!$this->fileLoaded) {
            return false;
        }
        $this->curRow++;
        $row = $this->fetchRow($this->curRow);
        if (count($row) > 0) {
            while (count($row) < count($this->headers)) {
                $row[] = null;
            }
            return array_combine($this->headers, $row);
        } else {
            // returns false if empty row
            return false;
        }
    }

}
