<?php

namespace App\Services;

use App\Models\Batch;
use App\Models\Company;
use App\Models\CompanyImage;
use App\Models\Contact;
use App\Models\ImportLookup;
use App\Models\QbCustomer;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Traits\Singleton;
use Illuminate\Support\Facades\DB;

class SeederService
{
    use Singleton;

    var $companyIdMap = [];

    private $keyMap = [
        'qb_customers' => [
            'source' => 'QB_CustomerList',
            'target' => QbCustomer::class,
            'map' => [
                'ListID' => 'qb_list_id',
                'Name' => 'company_name'
            ]
        ],

        'companies' => [
            'source' => 'Customers',
            'target' => Company::class,
            'map' => [
                'CompanyID' => 'company_code',
                'CompanyName' => 'company_name',
                'QBListID' => 'qb_list_id',

            ]
        ],

        'users' => [
            'source' => 'InternetSecurity',
            'target' => User::class,
            'map' => [
                'UserId' => 'username',
                'Password' => 'password',
                'CompanyId' => [
                    'function' => 'getCompanyId',
                    'target' => 'company_id',
                    'source' => 'CompanyId'
                ],
            ]
        ]
    ];

    private $cache = [];

    public function import($key)
    {
        print "Importing {$key}\n";
        $keyMap = $this->keyMap[$key];
        $sourceTable = $keyMap['source'];
        $targetModel = $keyMap['target'];
        $map = $keyMap['map'];
        $sql = "select " . implode(",", array_keys($map)) . " from {$sourceTable}";
        foreach (DB::connection('fe')->select($sql) as $row) {
            $data = [];
            foreach ($map as $source => $target) {
                if (is_array($target)) {
                    list($newTarget, $value) = $this->getCustomValue($row, $target);
                    $data[$newTarget] = $value;
                } else {
                    $data[$target] = $row->{$source};
                }
            }
            if ($key == 'users') {
                $contact = $this->buildContact($data['company_id'], $data['username']);
                $data['contact_id'] = $contact->id;
            }
            $targetModel::create($data);

            print ".";
        }
        print "\n\n";
    }

    private function getCustomValue($row, $rules)
    {
        if ($rules['function'] == 'getCompanyId') {
            return $this->getCompanyId($row, $rules);
        }
    }

    private function getCompanyId($row, $rules)
    {
        $key = $rules['target'];
        $source = $rules['source'];
        $companyId = $row->{$source};
        if (!isset($this->cache['companies'])) {
            $this->cache['companies'] = [];
        }
        if (!isset($this->cache['companies'][$companyId])) {
            $company = Company::where('company_code', $companyId)->first();
            if ($company === null) {
                $id = null;
            } else {
                $id = $company->id;
            }
            $this->cache['companies'][$companyId] = $id;
        }
        $value = $this->cache['companies'][$companyId];
        return [$key, $value];
    }

    private function buildContact($companyId, $firstName)
    {
        return Contact::create([
                                   'company_id' => $companyId,
                                   'first_name' => $firstName
                               ]);
    }

    public function assignRolesToUsers()
    {
        print "Applying User Roles\n";
        $roles = Role::all()->pluck('id', 'code');
        $adminRoles = [
            $roles['ADMIN'],
            $roles['COM-S'],
            $roles['COM-M'],
            $roles['COM-M'],
            $roles['PROV'],
        ];
        $userRoles = [
            $roles['COM-S'],
            $roles['COM-M'],
            $roles['COM-M'],
            $roles['PROV'],
        ];
        foreach (User::all() as $user) {
            print ".";
            if ($user->username == 'admin') {
                $roles = $adminRoles;
            } else {
                $roles = $userRoles;
            }
            foreach ($roles as $roleId) {
                UserRole::create(
                    [
                        'user_id' => $user->id,
                        'role_id' => $roleId
                    ]
                );
            }
        }
        print "\n\n";
    }

    public function generateAdmin()
    {
        $company = Company::create(
            [
                'company_code' => 'FEI',
                'company_name' => 'FEI Enterprises',
            ]
        );
        $contact = Contact::create(
            [
                'company_id' => $company->id,
                'first_name' => 'Sys',
                'last_name' => 'Admin'
            ]
        );
        User::create(
            [
                'username' => 'admin',
                'password' => 'fe1921',
                'company_id' => $company->id,
                'contact_id' => $contact->id
            ]
        );
    }

    public function importCompanyImages()
    {
        print "Importing Company Images\n";

        $sql = "
        select distinct c.CompanyID
        from CustomerImages ci
        inner join Images i on i.ID = ci.ImageID
        inner join Customers c on c.id = ci.CustomerID
        ";

        foreach (DB::connection('fe')->select($sql) as $row) {
            $sql = "
            select c.CompanyID, i.description, i.image
            from CustomerImages ci
            inner join Images i on i.ID = ci.ImageID
            inner join Customers c on c.id = ci.CustomerID
            where c.CompanyID = ?
            ";
            $imageRow = DB::connection('fe')->select($sql, [$row->CompanyID])[0];

            print ".";
            $company = Company::getByCompanyCode($imageRow->CompanyID);
            if ($company !== null) {
                $data = [
                    'company_id' => $company->id,
                    'label' => $imageRow->description,
                    'path' => $this->saveFile($imageRow->image)
                ];
                CompanyImage::create($data);
            }
        }
        print "\n\n";
    }

    private function saveFile($blob)
    {
        $fileName = $this->randomChars(32);
        $tmpPath = '/tmp/' . $fileName;
        file_put_contents($tmpPath, $blob);
        $fileType = $this->getFileType($tmpPath);

        $targetPath = storage_path();
        $targetPath .= '/app';
        if (!file_exists($targetPath)) {
            mkdir($targetPath);
        }
        $targetPath .= '/companyImages';
        if (!file_exists($targetPath)) {
            mkdir($targetPath);
        }
        $targetPath .= '/' . $fileName . '.' . $fileType;
        rename($tmpPath, $targetPath);
        return $targetPath;
    }

    private function getFileType($filePath)
    {
        $handle = @fopen($filePath, 'r');
        if (!$handle) {
            throw new \Exception('File Open Error');
        }

        $types = array(
            'jpeg' => "\xFF\xD8\xFF",
            'gif' => 'GIF',
            'png' => "\x89\x50\x4e\x47\x0d\x0a",
            'bmp' => 'BM',
            'psd' => '8BPS',
            'swf' => 'FWS'
        );
        $bytes = fgets($handle, 8);
        $found = 'other';
        foreach ($types as $type => $header) {
            if (strpos($bytes, $header) === 0) {
                $found = $type;
                break;
            }
        }
        fclose($handle);
        return $found;
    }

    private function randomChars($num)
    {
        $chars = '';
        $availableChars = str_split(
            '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        );
        for ($x = 0; $x < $num; $x++) {
            $chars .= $availableChars[rand(0, count($availableChars) - 1)];
        }
        return $chars;
    }

    private function getNextFileId()
    {
        // select min(id), max(id) from UploadedFiles
        $rows = DB::select('select min(source_id) source_id from import_lookups', ['UploadedFiles']);
        if ($rows[0]->source_id === null) {
            $rows = DB::connection('fe')->select("select max(id) as source_id from UploadedFiles");
        }

        return $rows[0]->source_id - 1;
    }

    public function importBatches($size)
    {
        $fileId = $this->getNextFileId();
        $timer = $size;
        while ($fileId > 0 && $timer > 0) {
            print $fileId . "\n";
            $this->importBatch($fileId);

            $fileId--;
            $timer--;
        }
    }

    public function getBatchByFileId($fileId)
    {
        $tables = [
            'BatchLog_Web' => [
                'min' => 10555,
                'max' => 21030000
            ],
            'BatchLogWeb_2018_07' => [
                'min' => 10573,
                'max' => 14956
            ],
            'BatLog_Web_2016'=> [
                'min' => 2,
                'max' => 10554
            ],
        ];
        $batches = $this->getTableByFileId($fileId, $tables);
        if ($batches === null) {
            $batches = [];
        }
        if (count($batches) == 0) {
            return null;
        }
        return $batches[0];
    }

    public function getBatchDetailByFileId($fileId)
    {
        $tables = [
            'BatchDetail_Web' => [
                'min' => 15788,
                'max' => 21030000
            ],
            'BatchDetail_Web_bak_20190101' => [
                'min' => 1,
                'max' => 15787
            ],
            'BatchDetail_Web_bak_20180109' => [
                'min' => 2,
                'max' => 9215
            ],
        ];
        return $this->getTableByFileId($fileId, $tables);
    }

    private function getTableByFileId($fileId, $tables)
    {
        $table = null;
        foreach ($tables as $tableId => $row) {
            if ($fileId >= $row['min'] && $fileId <= $row['max']) {
                $table = $tableId;
            }
        }
        if ($table === null) {
            return null;
        }
        $sql = 'select * from ' . $table . ' where fileId =?';
        return DB::connection('fe')->select($sql, [$fileId]);
    }

    private function addToLog($source, $sourceId, $target = null, $targetId = null)
    {
        $data = [
            'source' => $source,
            'source_id' => $sourceId,
            'target' => $target,
            'target_id' => $targetId,
        ];
        ImportLookup::create($data);
    }

    private function importBatch($fileId)
    {
        $this->addToLog('UploadedFiles', $fileId);

        $batch = $this->getBatchByFileId($fileId);
        if ($batch === null || !isset($batch->companyid)) {
            print "skipping: bad batch \n";
            dump($batch);
            return;
        }
        $companyId = $this->getCompanyIdFromCode($batch->companyid);
        if ($companyId === null) {
            print "skipping: no company id\n";
            return null;
        }
        $batchDetail = $this->getBatchDetailByFileId($fileId);

        if (count($batchDetail) > 0) {
            $batchData = [
                'company_id' => $companyId,
                'total_records' => $batch->TotalRecords,
                'total_amount' => $batch->TotalAmount,
                'status_id' => $batch->StatusID,
            ];
            $dbBatch = Batch::create($batchData);
            $this->addToLog('BatchLog_Web', $fileId, 'batches', $dbBatch->id);
            foreach ($batchDetail as $batchItem) {
                $data = [
                    'batch_id' => $dbBatch->id,
                    'ref' => $batchItem->REF,
                    'agency' => $batchItem->AGENCY,
                    'amount' => $batchItem->AMOUNT,
                    'first_name' => $batchItem->FIRST,
                    'last_name' => $batchItem->LAST,
                    'address1' => $batchItem->ADDRESS1,
                    'address2' => $batchItem->ADDRESS2,
                    'address3' => $batchItem->ADDRESS2,
                    'city' => $batchItem->CITY,
                    'state' => $batchItem->STATE,
                    'postal_code' => $batchItem->ZIP,
                    'country' => $batchItem->COUNTRY,
                    'iata' => $batchItem->IATA,
                    'in' => $batchItem->IN,
                    'out' => $batchItem->OUT,
                    'created_at' => $batchItem->DATEIMPORTED,
                    'updated_at' => $batchItem->DATEIMPORTED,
                ];
            }
        }
    }

    private function getCompanyIdFromCode($companyCode)
    {
        if (!isset($this->companyIdMap[$companyCode])) {
            $c = Company::getByCompanyCode($companyCode);
            if ($c === null) {
                $this->companyIdMap[$companyCode] = null;
            } else {
                $this->companyIdMap[$companyCode] = $c->id;
            }
        }
        return $this->companyIdMap[$companyCode] ?? null;
    }
}
