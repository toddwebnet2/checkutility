<?php

namespace App\Services;

use App\Models\Company;
use App\Models\User;

class AuthService
{

    public static function isLoggedIn()
    {
        return (is_numeric(session('userId')));
    }

    public static function getUserId()
    {
        return session('userId');
    }

    public static function storeUserInSession($username)
    {
        $request = request();
        $session = $request->session();
        $user = User::where('username', $username)->firstOrFail();

        $session->put('userId', $user->id);

        $session->put('companyCode', $user->company->company_code);
        $session->put('companyName', $user->company->company_name);
        $session->put('company_id', $user->company_id);
        $session->put('contact_name', trim($user->contact->first_name . ' ' . $user->contact->last_name));
        $session->put('permissions', $user->permissions());
    }

    public static function hasPermission($permission)
    {
        $permissions = self::getPermissions();
        return in_array($permission, $permissions);
    }

    public static function sessionVar($key)
    {
        return session($key);
    }

    public static function logout()
    {
        $request = request();
        $session = $request->session();
        $session->flush();
    }

    public static function getPermissions()
    {
        static $permissions;
        if ($permissions === null) {
            $permissions = self::sessionVar('permissions');
        }
        return $permissions;
    }

}
