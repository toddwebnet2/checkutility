<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AuthService;
use App\Services\FlashService;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function index()
    {
        return view('login');
    }

    public function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');


        if (!User::isValidLogin($username, $password)) {
            FlashService::setFlashMessage('error', __('message.invalidCredentials'));
        } else {
            AuthService::storeUserInSession($username);
        }
        return redirect(app_url() .'/');
    }

    public function logout(){
        AuthService::logout();
        FlashService::setFlashMessage('info', __('message.loggedOut'));
        redirect("/");
    }
}
