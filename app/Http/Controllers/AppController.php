<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AuthService;
use App\Services\FlashService;
use App\Services\ShoppingCartService;
use Illuminate\Http\Request;


class AppController extends Controller
{
    public function index()
    {
        if (AuthService::isLoggedIn()) {
            return redirect("/dashboard");
        } else {
            return redirect("/login");
        }
    }


    public function dashboard()
    {
        $links = [];
        $appUrl = env('app_url');

        if (AuthService::hasPermission('admin')) {
            $links[] = [
                'link' => $appUrl . '/admin/users',
                'label' => __('fei.user_administration')
            ];
            $links[] = [
                'link' => $appUrl . '/admin/pp',
                'label' => __('fei.positive_pay')
            ];
        }
        if (            AuthService::hasPermission('commissions-single')){
            $links[] = [
                'link' => $appUrl . '/commissions/upload',
                'label' => __('fei.commissions_upload')
            ];
        }
        if (
            AuthService::hasPermission('commissions-single') ||
            AuthService::hasPermission('commissions-multi')
        ) {
            $links[] = [
                'link' => $appUrl . '/commissions',
                'label' => __('fei.commissions')
            ];
        }
        if (AuthService::hasPermission('proveedores')) {
            $links[] = [
                'link' => $appUrl . '/commissions',
                'label' => __('fei.proveedores')
            ];
        }

        $links[] = [
            'link' => $appUrl . '/cobranza',
            'label' => __('fei.cobranza')
        ];
        $data = ['links' => $links];
        return view('dashboard', $data);
    }
}
