<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Services\AuthService;
use App\Services\FlashService;
use App\Services\UploadService;
use Illuminate\Http\Request;

class CommissionsController extends Controller
{
    public function upload()
    {
        $companyId = AuthService::sessionVar('company_id');
        $company = Company::find($companyId);
        $data = [
            'companyCode' => $company->company_code,
            'companyName' => $company->company_name
        ];
        return view('commissions.upload', $data);
    }

    public function procUpload(Request $request)
    {
        $uploadService = UploadService::instance();
        $file = $request->file('upload');

        if ($file === null) {
            FlashService::setFlashMessage('error', 'no file was uploaded');
            return redirect('/commissions/upload');
        }
        if (!$uploadService->validateFileType($file)) {
            FlashService::setFlashMessage(
                'error',
                'Invalid file type (not csv, xlsx, or xls) - content did not match a valid file'
            );
            return redirect('/commissions/upload');
        }

        $data = $uploadService->transformUpload($file);
        $numRows = 0;
        $total = 0;
        foreach ($data['data'] as $row) {
            $total += $row['AMOUNT'];
            $numRows++;
        }
        $data['total'] = $total;
        $data['numRows'] = $numRows;
        return view('commissions/validate', $data);
    }

    public function procValidatedUpload(Request $request)
    {
        $batch = UploadService::instance()->postNewBatches(
            $request->post('company_id'),
            $request->post('total_records'),
            $request->post('total_amount'),
            $request->post('upload_file_id')
        );
        FlashService::setFlashMessage('success', __('com.batch_saved'));
        return view('commissions/completed', ['batch' => $batch]);
    }
}
