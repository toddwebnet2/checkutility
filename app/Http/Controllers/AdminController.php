<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyImage;
use App\Models\Contact;
use App\Models\User;
use App\Services\FlashService;
use Illuminate\Http\Request;

class AdminController
{

    public function users()
    {
        $data = [
            'companies' => Company::orderBy('company_name')->get()->load(['users', 'image'])
        ];
        return view('admin/users', $data);
    }

    public function image($imageId)
    {
        $file = CompanyImage::find($imageId);
        $ext = substr($file->path, strrpos($file->path, '.') + 1);
        header('Content-type: image/' . $ext);
        readfile($file->path);
    }

    public function user($userId)
    {
        $data = [
            'user' => User::find($userId),
            'companies' => Company::all()->keyBy('id')
        ];
        return view('admin/user', $data);
    }

    public function addUser($companyId)
    {
        $user = (object)[
            'id' => 0,
            'contact_id' => 0,
            'company_id' => $companyId,
            'username' => '',
            'password' => '',
            'note' => null,
            'language' => 'en',
            'company' => (object)[
                'id' => 0,
                'image' => null
            ]
        ];

        $data = [
            'user' => $user,
            'companies' => Company::all()->keyBy('id')
        ];
        return view('admin/user', $data);
    }

    public function saveUser(Request $request)
    {
        $userId = $request->input('id');
        $companyId = $request->input('company_id');
        $imageId = $request->input('image_id');
        $companyName = $request->input('company_name');
        $companyCode = $request->input('company_code');
        $imageDescription = $request->input('image_description');
        $username = $request->input('username');
        $password = $request->input('password');

        if (User::where('id', $userId)
                ->where('username', '!=', $request->input('username'))
                ->count() > 0) {
            FlashService::setFlashMessage('error', 'User Name already exists');

            if ($userId == 0) {
                return redirect('/admin/user/company/' . $companyId);
            } else {
                return redirect('/admin/user' . $userId);
            }
        }

        if ($companyId == 0) {
            $companyObject = Company::create(
                [
                    'company_code' => $companyCode,
                    'company_name' => $companyName,
                ]
            );
            $companyId = $companyObject->id;
        }

        $hasFile = false;
        if ($request->file('image')) {
            $hasFile = true;

            $file = $request->file('image');
            $path = CompanyImage::copyImage($file->getPathname());

            if ($imageId == 0) {
                $image = CompanyImage::create(
                    [
                        'company_id' => $companyId,
                        'label' => $imageDescription,
                        'path' => $path,
                    ]
                );
                $image->save();
            } else {
                $image = CompanyImage::find($imageId);
                $image->company_id = $companyId;
                $image->label = $imageDescription;
                $image->path = $path;
                $image->save();
            }
            $imageId = $image->id;
        }
        $companyData = [
            'company_code' => $companyCode,
            'company_name' => $companyName,
        ];
        if ($companyId == 0) {
            $customer = Company::create($companyData);
            $customer->save();
        } else {
            $company = Company::find($companyId);
            foreach ($companyData as $field => $value) {
                $company->{$field} = $value;
            }
            $company->save();
        }
        $companyId = $company->id;
        $customer = Company::find($companyId);

        $userId = $request->input('id');
        if ($userId == 0) {
            $contact = Contact::create(['first_name', $username]);
            $user = User::create(
                [
                    'contact_id' => $contact->id,
                    'username' => $username,
                    'password' => $password,
                    'company_id' => $companyId,
                ]
            );
            $user->save();
        } else {
            $user = User::find($userId);
            $user->username = $username;
            $user->password = $password;
            $user->company_id = $companyId;
            $user->save();
        }

        FlashService::setFlashMessage('info', "Information Saved");
        return redirect('/admin/users');
    }

    function pp()
    {
        return view('admin.pp');
    }

    function ajaxCommand($bank)
    {
        return view('admin.pp_ajax_commands', ['bank' => $bank]);
    }
}
