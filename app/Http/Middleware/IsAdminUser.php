<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use Illuminate\Http\Request;

class IsAdminUser
{
    public function handle(Request $request, \Closure $next)
    {
        if (!AuthService::hasPermission('admin')) {
            return redirect('/dashboard');
        }
        return $next($request);
    }
}
