<?php

namespace App\Http\Middleware;

use App\Services\AuthService;
use Illuminate\Http\Request;


class IsLoggedIn
{
    public function handle(Request $request, \Closure $next)
    {
        if (!AuthService::isLoggedIn()) {
            return redirect('/');
        }
        return $next($request);
    }
}
