<?php

namespace App\Console\Commands;

use App\Services\SeederService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ImportCommand extends Command
{
    protected $signature = 'import';

    public function handle()
    {
        Artisan::call('migrate:refresh');
        SeederService::instance()->generateAdmin();
        SeederService::instance()->import('qb_customers');
        SeederService::instance()->import('companies');
        SeederService::instance()->import('users');
        SeederService::instance()->assignRolesToUsers();
        SeederService::instance()->importCompanyImages();

    }
}
