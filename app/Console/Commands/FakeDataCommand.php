<?php

namespace App\Console\Commands;

use Faker\Factory as Faker;
use Illuminate\Console\Command;

class FakeDataCommand extends Command
{
    protected $signature = 'fake';

    public function handle()
    {
        $keys = explode(
            ",",
            "REF,AGENCY,AMOUNT,ADDRESS1,ADDRESS2,ADDRESS3,CITY,STATE,ZIP,COUNTRY,HOTEL,FIRST,LAST,IN,OUT,IATA"
        );
        $rows = [];
        $rows[] = $keys;
        for ($x = 0; $x < 25; $x++) {
            $rows[] = $this->generateRow($keys);
        }
        print "\n\n";
        foreach ($rows as $row) {
            print implode(",", $row) . "\n";
        }
        print "\n\n";
    }

    private function generateRow($keys)
    {
        $faker = Faker::create();
        static $agencies;
        static $persons;
        if ($agencies === null) {
            $agencies = [];
            for ($x = 0; $x < 20; $x++) {
                $agencies[] = $faker->lastName . ' Travel';
            }
        }

        if ($persons === null) {
            $persons = [];
            for ($x = 0; $x < 50; $x++) {
                $persons[] = [
                    'address' => $faker->streetAddress,
                    'postcode' => $faker->postcode,
                    'city' => $faker->city,
                    'firstName' => $faker->firstName,
                    'lastName' => $faker->lastName,
                ];
            }
        }

        $agency = $agencies[rand(0, count($agencies)-1)];
        $person = $persons[rand(0, count($persons)-1)];

        $days = rand(1, 15);
        $in = date('Y-m-d', strtotime(rand(1, 255) . ' days ago'));
        $out = date('Y-m-d', strtotime($in . " + {$days} days"));

        $ref = rand(10000, 99999);
        $data = [];
        foreach ($keys as $key) {
            switch ($key) {
                case 'REF':
                    $data[] = (string)$ref++;
                    break;
                case 'AGENCY':
                    $data[] = $agency;
                    break;
                case 'AMOUNT':
                    $data[] = $days * 25;
                    break;
                case 'ADDRESS1':
                    $data[] = $person['address'];
                    break;
                case 'ADDRESS2':
                case 'ADDRESS3':
                case 'COUNTRY':
                    $data[] = 'USA';
                    break;
                case 'CITY':
                    $data[] = $person['city'];
                    break;
                case 'STATE':
                    $data[] = 'TX';
                    break;
                case 'ZIP':
                    $data[] = $person['postcode'];
                    break;
                case 'HOTEL':
                    $data[] = $faker->lastName . ' Hotel';
                    break;
                case 'FIRST':
                    $data[] = $person['firstName'];
                    break;
                case 'LAST':
                    $data[] = $person['lastName'];
                    break;
                case 'IN':
                    $data[] = $in;
                    break;
                case 'OUT':
                    $data[] = $out;
                    break;
                case 'IATA':
                    $data[] = (string)rand(10000000, 99999999);
                    break;
            }
        }
        return $data;
    }
}
