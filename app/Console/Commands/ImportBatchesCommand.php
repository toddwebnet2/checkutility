<?php

namespace App\Console\Commands;

use App\Services\SeederService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ImportBatchesCommand extends Command
{
    protected $signature = 'import-batches';

    public function handle()
    {
        $size = 1000000;
        SeederService::instance()->importBatches($size);

    }
}
