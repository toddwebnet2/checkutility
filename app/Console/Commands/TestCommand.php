<?php

namespace App\Console\Commands;

use App\Services\ExcelReaderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TestCommand extends Command
{
    protected $signature = 'test';

    public function handle()
    {
       $rows = DB::connection('fe')->select("select * from sysobjects where xtype = 'u' ");
       dd($rows);

    }
}
