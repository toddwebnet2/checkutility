<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CheckingAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checking_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_name', 32);
            $table->string('qb_list_id', 32);
            $table->string('account_number', 64)->nullable();;
            $table->string('aba', 16);
            $table->string('check_name', 128)->nullable();
            $table->string('report_name', 128)->nullable();
            $table->bigInteger('check_series')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checking_accounts');
    }
}
