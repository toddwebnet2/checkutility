<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descr', 32);
            $table->boolean('is_active')->default(true);
            $table->timestamps();

        });

        $this->populate();
    }

    private function populate()
    {
        $sqlInsert = "insert into products (id, descr, is_active) values ";
        $sqls = [
            "{$sqlInsert} (1, 'Comisiones', 1)",
            "{$sqlInsert} (2, 'Proveedores', 1)",
            "{$sqlInsert} (3, 'Palmilla', 0)",
            "{$sqlInsert} (4, 'Comisiones New', 0)",
            "{$sqlInsert} (5, 'Comissiones Refunds New', 0)",
        ];
        foreach ($sqls as $sql) {
            \Illuminate\Support\Facades\DB::insert($sql);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
