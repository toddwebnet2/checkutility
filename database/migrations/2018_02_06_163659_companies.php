<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Companies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qb_customers', function (Blueprint $table) {
            $table->string('qb_list_id', 128)->nullable();
            $table->string('company_name', 128);
            $table->primary('qb_list_id');
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code', 32);
            $table->string('company_name', 128);
            $table->string('qb_list_id', 128)->nullable();
//            $table->string('customer_type', 128);
            $table->boolean('is_active')->default(true);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::dropIfExists('qb_customers');

    }
}
