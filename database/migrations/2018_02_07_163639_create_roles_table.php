<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->unsigned();;
            $table->string('code', 16);
            $table->string('name', 32);
            $table->tinyText('permissions');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        $roles = [
            [
                'code' => 'ADMIN',
                'name' => 'Administrator',
                'permissions' => ['admin']
            ],
            [
                'code' => 'COM-S',
                'name' => 'Single Commissions',
                'permissions' => ['commissions-single']
            ],
            [
                'code' => 'COM-M',
                'name' => 'Multi Commissions',
                'permissions' => ['commissions-multi']
            ],
            [
                'code' => 'COBRA',
                'name' => 'Cobranza',
                'permissions' => ['cobranza']
            ],
            [
                'code' => 'PROV',
                'name' => 'Proveedores',
                'permissions' => ['proveedores']
            ]
        ];

        foreach ($roles as $role) {
            \App\Models\Role::create($role);
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
