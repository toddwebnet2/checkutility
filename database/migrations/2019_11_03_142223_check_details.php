<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CheckDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('check_id');
            $table->unsignedInteger('company_id');
            $table->unsignedBigInteger('batch_id');

            $table->string('ref', 255)->default('');
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('hotel', 255);
            $table->dateTime('in')->nullable();
            $table->dateTime('out')->nullable();
            $table->decimal('amount', 18, 2);

            $table->string('rec', 255 )->default('');




            $table->timestamps();


            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('check_id')->references('id')->on('checks');
            $table->foreign('batch_id')->references('id')->on('batches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_details');

    }
}
