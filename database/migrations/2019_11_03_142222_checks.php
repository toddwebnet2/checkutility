<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Checks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            // cust id
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('checking_account_id');
            // job id
            $table->unsignedBigInteger('batch_id')->nullable();
            // refNumber
            $table->bigInteger('check_number');
            $table->decimal('amount', 18, 2);

            $table->text('memo');
            $table->text('rec');
            $table->string('transaction_id', 128)->nullable();
            $table->string('transaction_number', 128)->nullable();
            $table->dateTime('transaction_date')->nullable();
            $table->string('edit_sequence', 32)->nullable();

            $table->string('addr_1', 255)->nullable();
            $table->string('addr_2', 255)->nullable();
            $table->string('addr_3', 255)->nullable();
            $table->string('addr_4', 255)->nullable();

            $table->string('addr_city', 255)->nullable();
            $table->string('addr_state', 255)->nullable();
            $table->string('addr_postal_code', 128)->nullable();
            $table->string('addr_country', 128)->nullable();

            $table->string('account_ref_list_id', 255)->nullable();
            $table->string('account_ref_full_name', 255)->nullable();
            $table->string('payee_entity_ref_list_id', 255)->nullable();
            $table->string('payee_entity_ref_full_name', 255)->nullable();

            $table->string('iata', 128)->nullable();

            // IsToBePrinted, Printed
            $table->enum('print_status', ['isToBePrinted', 'Printed', 'Canceled'])->nullable();
            $table->timestamps();


            $table->foreign('batch_id')->references('id')->on('batches');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('checking_account_id')->references('id')->on('checking_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checks');
    }
}
