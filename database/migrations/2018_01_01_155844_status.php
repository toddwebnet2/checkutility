<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Status extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status', 16);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        $this->populate();
    }

    private function populate()
    {
        $insertSql = "insert into status (id, status) values ";
        $sqls = [
            "{$insertSql} (1, 'Pending')",
            "{$insertSql} (2, 'Processing')",
            "{$insertSql} (3, 'Printing')",
            "{$insertSql} (4, 'Packing')",
            "{$insertSql} (5, 'Shipped')",
            "{$insertSql} (6, 'Cancelled')",
            "{$insertSql} (7, 'Invoiced')",
            "{$insertSql} (8, 'Paid')",
            "{$insertSql} (9, 'Returned')"
        ];
        foreach($sqls as $sql){
            \Illuminate\Support\Facades\DB::insert($sql);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
