<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QbCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qb_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qb_file', 255);
            $table->string('name', 128);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        $this->populate();

    }

    private function populate(){
        $insertSql = "insert into qb_companies (id, qb_file, name) values ";
        $sqls = [
            "{$insertSql} (1, 'C:\Program Files\QTiCheckUtility\Fe.qbw', 'FE Financial')",
            "{$insertSql} (2, 'C:\Program Files\QTiCheckUtility\Fei.qbw', 'Fei Enterprises, Inc.')",
        ];
        foreach($sqls as $sql){
            \Illuminate\Support\Facades\DB::insert($sql);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qb_companies');
    }
}
