<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('contact_id')->unsigned();
            $table->unsignedInteger('company_id')->unsigned()->nullable();
            $table->string('username', 32)->unique();
            $table->string('password', 2048)->nullable();
            $table->string('note')->nullable();
            $table->enum('language', ['en', 'es'])->default('en');
            $table->boolean('is_active')->default(true);

            $table->timestamps();

            $table->index('username');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::drop('user_roles');
        Schema::drop('users');
    }
}
