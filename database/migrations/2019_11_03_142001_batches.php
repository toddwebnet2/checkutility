<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Batches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->unsignedInteger('qb_company_id')->nullable();
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('uploaded_file_id')->nullable();
            $table->unsignedInteger('total_records')->default(0);
            $table->decimal('total_amount', 18,2);
            $table->string('file_path', 255)->nullable();
            $table->unsignedInteger('status_id')->default(1);
            $table->timestamps();


            $table->foreign('status_id')->references('id')->on('status');
//            $table->foreign('qb_company_id')->references('id')->on('qb_companies');
            $table->foreign('company_id')->references('id')->on('companies');
//            $table->foreign('uploaded_file_id')->references('id')->on('uploaded_files');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batches');
    }
}
