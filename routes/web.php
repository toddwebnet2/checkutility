<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\CommissionsController;
use App\Http\Controllers\LoginController;

use App\Http\Middleware\IsAdminUser;
use App\Http\Middleware\IsLoggedIn;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AppController::class, 'index']);
Route::get('/login', [LoginController::class, 'index']);
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::middleware([IsLoggedIn::class])->group(function () {
    Route::get('/dashboard', [AppController::class, 'dashboard']);
    Route::get('/commissions/upload', [CommissionsController::class, 'upload']);
    Route::post('/commissions/upload', [CommissionsController::class, 'procUpload']);
    Route::post('/commissions/validate', [CommissionsController::class, 'procValidatedUpload']);


    Route::middleware([IsAdminUser::class])->group(function () {
        Route::get('/admin/users', [AdminController::class, 'users']);
        Route::get('/admin/image/{imageId}', [AdminController::class, 'image']);
        Route::post('/admin/user', [AdminController::class, 'saveUser']);
        Route::get('/admin/user/company/{companyId}', [AdminController::class, 'addUser']);
        Route::get('/admin/user/{userId}', [AdminController::class, 'user']);


        Route::get('/admin/pp', [AdminController::class, 'pp']);
        Route::get('/admin/ajax/commands/{bank}', [AdminController::class, 'ajaxCommand']);
    });
});
